---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 3500 |
| [@leipert](https://gitlab.com/leipert) | 2 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1510 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 1300 |
| [@djadmin](https://gitlab.com/djadmin) | 5 | 980 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 6 | 940 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 7 | 900 |
| [@brodock](https://gitlab.com/brodock) | 8 | 800 |
| [@toon](https://gitlab.com/toon) | 9 | 700 |
| [@.luke](https://gitlab.com/.luke) | 10 | 640 |
| [@kassio](https://gitlab.com/kassio) | 11 | 600 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 12 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 13 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 14 | 600 |
| [@Alexand](https://gitlab.com/Alexand) | 15 | 600 |
| [@ifarkas](https://gitlab.com/ifarkas) | 16 | 520 |
| [@ratchade](https://gitlab.com/ratchade) | 17 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 18 | 500 |
| [@jlear](https://gitlab.com/jlear) | 19 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 20 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 21 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 22 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 23 | 500 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 24 | 500 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 25 | 490 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 26 | 480 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 480 |
| [@kerrizor](https://gitlab.com/kerrizor) | 28 | 480 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 29 | 460 |
| [@garyh](https://gitlab.com/garyh) | 30 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 31 | 440 |
| [@stanhu](https://gitlab.com/stanhu) | 32 | 400 |
| [@xanf](https://gitlab.com/xanf) | 33 | 400 |
| [@markrian](https://gitlab.com/markrian) | 34 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 35 | 300 |
| [@dzubova](https://gitlab.com/dzubova) | 36 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 37 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 38 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 39 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 40 | 300 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 41 | 290 |
| [@ahegyi](https://gitlab.com/ahegyi) | 42 | 260 |
| [@vshushlin](https://gitlab.com/vshushlin) | 43 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 44 | 220 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 45 | 210 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 46 | 200 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 47 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 48 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 49 | 190 |
| [@10io](https://gitlab.com/10io) | 50 | 170 |
| [@pshutsin](https://gitlab.com/pshutsin) | 51 | 140 |
| [@avielle](https://gitlab.com/avielle) | 52 | 140 |
| [@mwoolf](https://gitlab.com/mwoolf) | 53 | 130 |
| [@alexpooley](https://gitlab.com/alexpooley) | 54 | 130 |
| [@egrieff](https://gitlab.com/egrieff) | 55 | 120 |
| [@lauraX](https://gitlab.com/lauraX) | 56 | 120 |
| [@dblessing](https://gitlab.com/dblessing) | 57 | 120 |
| [@mattkasa](https://gitlab.com/mattkasa) | 58 | 100 |
| [@drew](https://gitlab.com/drew) | 59 | 90 |
| [@dmakovey](https://gitlab.com/dmakovey) | 60 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 61 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 62 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 63 | 80 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 65 | 60 |
| [@minac](https://gitlab.com/minac) | 66 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 67 | 60 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 68 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 69 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 70 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 71 | 60 |
| [@cngo](https://gitlab.com/cngo) | 72 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 73 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 74 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 75 | 60 |
| [@abdwdd](https://gitlab.com/abdwdd) | 76 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 77 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 78 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 79 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 80 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 81 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 82 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 83 | 30 |
| [@subashis](https://gitlab.com/subashis) | 84 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 85 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 86 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 87 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 88 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 89 | 30 |
| [@acroitor](https://gitlab.com/acroitor) | 90 | 30 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 91 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 92 | 20 |
| [@terrichu](https://gitlab.com/terrichu) | 93 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@greg](https://gitlab.com/greg) | 2 | 500 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 3 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 440 |
| [@f_santos](https://gitlab.com/f_santos) | 5 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 6 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 7 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 8 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 9 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 10 | 30 |
| [@fneill](https://gitlab.com/fneill) | 11 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 2 | 400 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 3 | 400 |
| [@vburton](https://gitlab.com/vburton) | 4 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@mehulsharma](https://gitlab.com/mehulsharma) | 2 | 500 |
| [@feistel](https://gitlab.com/feistel) | 3 | 400 |
| [@tnir](https://gitlab.com/tnir) | 4 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 5 | 300 |
| [@kyrie.31415926535](https://gitlab.com/kyrie.31415926535) | 6 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 7 | 200 |
| [@zined](https://gitlab.com/zined) | 8 | 200 |
| [@trakos](https://gitlab.com/trakos) | 9 | 200 |

## FY23-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 2300 |
| [@sabrams](https://gitlab.com/sabrams) | 2 | 1300 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 3 | 900 |
| [@toon](https://gitlab.com/toon) | 4 | 700 |
| [@Alexand](https://gitlab.com/Alexand) | 5 | 600 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 6 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 7 | 480 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 8 | 430 |
| [@ifarkas](https://gitlab.com/ifarkas) | 9 | 420 |
| [@markrian](https://gitlab.com/markrian) | 10 | 400 |
| [@dzubova](https://gitlab.com/dzubova) | 11 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 12 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 13 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 14 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 15 | 300 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 16 | 260 |
| [@ahegyi](https://gitlab.com/ahegyi) | 17 | 260 |
| [@toupeira](https://gitlab.com/toupeira) | 18 | 240 |
| [@vshushlin](https://gitlab.com/vshushlin) | 19 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 20 | 220 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 21 | 200 |
| [@brodock](https://gitlab.com/brodock) | 22 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 23 | 200 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 24 | 180 |
| [@avielle](https://gitlab.com/avielle) | 25 | 140 |
| [@drew](https://gitlab.com/drew) | 26 | 90 |
| [@kerrizor](https://gitlab.com/kerrizor) | 27 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 28 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 29 | 80 |
| [@.luke](https://gitlab.com/.luke) | 30 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 31 | 70 |
| [@10io](https://gitlab.com/10io) | 32 | 70 |
| [@egrieff](https://gitlab.com/egrieff) | 33 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 34 | 60 |
| [@tkuah](https://gitlab.com/tkuah) | 35 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 36 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 37 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 38 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 39 | 60 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 40 | 60 |
| [@abdwdd](https://gitlab.com/abdwdd) | 41 | 60 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 42 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 43 | 40 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 44 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 45 | 40 |
| [@alexpooley](https://gitlab.com/alexpooley) | 46 | 30 |
| [@mbobin](https://gitlab.com/mbobin) | 47 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 48 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 49 | 30 |
| [@acroitor](https://gitlab.com/acroitor) | 50 | 30 |
| [@terrichu](https://gitlab.com/terrichu) | 51 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 2 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 3 | 40 |
| [@fneill](https://gitlab.com/fneill) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 2 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@mehulsharma](https://gitlab.com/mehulsharma) | 1 | 500 |
| [@tnir](https://gitlab.com/tnir) | 2 | 400 |
| [@kyrie.31415926535](https://gitlab.com/kyrie.31415926535) | 3 | 300 |
| [@trakos](https://gitlab.com/trakos) | 4 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 22 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 23 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 26 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@10io](https://gitlab.com/10io) | 31 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 33 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 34 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 35 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 36 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 38 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 39 | 60 |
| [@minac](https://gitlab.com/minac) | 40 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 41 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 42 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 43 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 44 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 45 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 46 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 60 |
| [@cngo](https://gitlab.com/cngo) | 48 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 49 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 51 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 52 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 53 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 54 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 55 | 30 |
| [@subashis](https://gitlab.com/subashis) | 56 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 57 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 59 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 60 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 61 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 62 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 63 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 65 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 3 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 4 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 5 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 6 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 7 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@vburton](https://gitlab.com/vburton) | 2 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |


