---
layout: handbook-page-toc
title: "Fast decisions — Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/fast-decisions/"
description: Fast decisions — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

<%= partial("handbook/mecc/_mecc_overview.erb") %>

# Fast Decisions

Fast decisions are a key attribute of [MECC](/handbook/mecc/). While other management philosophies prioritize the speed of knowledge ***transfer***, MECC optimizes for the speed of knowledge ***retrieval***.  

Below are the tenets of Fast Decisions within MECC. 

## Iteration

In conventional organizations, there's often inherent pressure to present a complete and polished project, document, or plan. This expectation slows progress and expends valuable time that could be used to exchange multiple rounds of feedback on smaller changes. 

A key aspect of MECC is incorporating [iteration](/handbook/values/#iteration) into every process and decision with a [low level of shame](/handbook/values/#low-level-of-shame). This means doing the smallest thing possible and getting it out as quickly as possible. Despite the initial discomfort that comes from sharing the [minimal viable change (MVC)](/handbook/values/#minimal-viable-change-mvc), iteration enables faster execution, a shorter feedback loop, and the ability to course correct sooner. 

Here's an example:

- _In a conventional organization:_ A team member spends multiple weeks perfecting a new project to present a polished, final copy to the broader team.  
- _In an organization empowered by MECC:_ A team member starts work on a new idea right away by making the minimal viable change (MVC) and sharing it with their team for feedback. At GitLab, we [make small merge requests](/handbook/values/#make-small-merge-requests), allowing the team to incorporate input early and often and be sure they're working in the right direction. 

## Short toes 

An organization's speed of decision making can be dramatically slowed if teams are concerned about "stepping on others' toes" by contributing to work outside of their immediate job description. This is often fueled by a fear of conflict, which is one of the [five dysfunctions](/handbook/values/#five-dysfunctions) of a team.

Adopting a MECC mentality means having [short toes](/handbook/values/#short-toes) and feeling comfortable with others taking initiative to contribute to your domain. Eliminating a territorial mindset allows for better [collaboration](/handbook/values/#collaboration), more [diversity of thought](/handbook/values/#seek-diverse-perspectives), and ultimately faster decisions.  

Here's an example:

- _In a conventional organization:_ The CEO announces a new name to refer to people who work at the company. Team members disagree with the decision, but they feel unable to speak up because this decision falls outside of their domain. This leads to a breakdown of trust and poor adoption of the decision itself. 
- _In an organization empowered by MECC:_ GitLab's CEO makes [a merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/24447) to create a name for people who work at the company. After pointed, respectful feedback to the proposal from team members in multiple departments, he closes the merge request.  


## Proposals

Traditional organizations often use brainstorming meetings as a crutch, gathering input from a group before taking action. MECC unlocks faster decision making by requiring that individuals [make a proposal](/handbook/values/#make-a-proposal) first. This [bias for action](/handbook/values/#bias-for-action) means that valuable time spent in meetings will be more productive and efficient because participants are able to react to something concrete and provide feedback, or even alternative proposals.  

In a MECC organization, [everything is in draft](/handbook/values/#everything-is-in-draft) by default. Establishing a shared understanding that everything is subject to change removes red tape and [politics](/handbook/values/#playing-politics-is-counter-to-gitlab-values) and creates an environment where teams can ship smaller changes, faster.  

Here's an example:

- _In a conventional organization:_ If a team member disagrees with a given policy, they stew or rally a clique. The only meaningful avenue for change is to leverage political savvy to lobby the right people into creating or facilitating change. 
- _In an organization empowered by MECC:_ When a team member disagrees with a given policy, they are asked to [make a proposal](/handbook/values/#make-a-proposal) to improve the situation. This enables everyone to contribute to solutions, and reduces the effectiveness of politics and complaining without solutioning. If said team member realizes that they cannot document a superior proposal, the attempt itself may diffuse their frustration.


## Directly responsible individual (DRI) 

Another foundational element to MECC is that each project or decision is assigned a [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/) who is ultimately responsible for its success or failure. This prevents the risk of sluggish decision-making that conventional organizations often experience when there are unclear expectations and too many people involved. 

Leaders must foster a culture where DRIs are empowered, able to [escalate to unblock](/handbook/values/#escalate-to-unblock), and willing to share their ideas in the open. This unlocks the team's highest potential. A successful DRI should consult and collaborate with all teams and stakeholders and welcome input from a broad range of diverse perspectives as they form their thoughts. 

It's important to note that MECC still allows flexibility for team members to [disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree), but it reduces the risk that disagreement or dissent will prevent a [bias for action](/handbook/values/#bias-for-action). 

Here's an example:

- _In a conventional organization:_ If a team member wishes to change something, their degree of success hinges first on their ability to [gather consensus](/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making). The quality of the proposal takes a backseat to the quality of one's lobbying abilities. 
- _In an organization empowered by MECC:_ If a team member wishes to change something, and they are chosen as the [DRI](/handbook/people-group/directly-responsible-individuals/) by their manager or project/functional leader, everyone can contribute to the success of the DRI by offering input as they steward a project or goal. Consensus becomes *less* relevant. The quality of each iteration becomes *more* relevant. A DRI's ability to foster, parse, analyze, and integrate contributions from everyone largely defines their success as the leader, *not* their ability to gather consensus. 


## Informal communication to build trust

An intentional approach to [informal communication](/company/culture/all-remote/informal-communication/) is crucial in a fast-paced organization with a bias for [asynchronous workflows](/company/culture/all-remote/asynchronous) and [text-based communication](/company/culture/all-remote/effective-communication/). Leaders should encourage team members to prioritize informal connections (e.g. coffee chats, social calls, special interest chat channels) and [get to know the people](/handbook/values/#get-to-know-each-other) behind the text. This builds trust, prevents conflict, and enables better communication during work-related interactions. 

Building this level of trust also helps enable DRIs to make faster decisions, as the there's a foundation of confidence in the experience and judgment of others. 

Here's an example:

- _In a conventional organization:_ Two members of a project team have never worked together before, and one of them is the DRI for the project. A lack of trust and common understanding of the other person's background causes them to run into a number of roadblocks, and slows the project's pace.   
- _In an organization empowered by MECC:_ Two GitLab team members have never worked together before, so they set up a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) and exchange [READMEs](/handbook/engineering/readmes/) prior to a new project starting. They learn a lot about each other, their work styles, and their backgrounds in the 25-minute video call and the asynchronous README reviews prior. The project runs more smoothly because of their [shared trust](/handbook/leadership/building-trust/) beyond the transactional work interactions.

## Two-way door decisions

In MECC, decisions are [two-way doors](/handbook/values/#make-two-way-door-decisions), meaning they're easy to reverse. That's why a DRI should go ahead and make the decision without approval or consensus. The only time a decision should require a more thorough discussion first is when you can't reverse it ***or*** break it down into smaller, reversable components. 

This requires a reframing of the conventional management mindset. Reverting work back to a previous state is a positive thing, because you're quickly getting feedback and learning from it. Making a small change quickly prevents a bigger revert in the future. 

Here's an example:

- _In a conventional organization:_ A recruiting leader holds a brainstorming meeting to discuss an update to the hiring process. 
- _In an organization empowered by MECC:_ A recruiting leader makes a merge request to update part of the hiring process in the company handbook. They share the MR with the team, knowing that the decision can be reverted if needed. This enables everyone to contribute to making the proposal better and the iteration stronger.

---

Fast decisions enable [Many decisions](/handbook/mecc/many-decisions), the third tenet of [MECC](/handbook/mecc/). 

